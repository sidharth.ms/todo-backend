FROM gradle:7.6-jdk17 AS build
WORKDIR /home/gradle/project
COPY . .
RUN gradle build --no-daemon
