package com.example.backend;


import javax.persistence.*;

@Entity
@Table(name = "todo")
public class ToDoList {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "discription")
    private String discription;
    @Column(name = "completed")
    private Boolean completed;

    ToDoList(String discription, Boolean completed){
        this.discription = discription;
        this.completed = completed;
    }

    public ToDoList() {
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDiscription() {
        return discription;
    }

    public Long getId() {
        return id;
    }

    public void setDiscription(String discription) {
        this.discription = discription;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }
}
