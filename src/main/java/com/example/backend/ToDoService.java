package com.example.backend;

import org.springframework.stereotype.Service;

@Service
public class ToDoService {
    ToDoListInterface toDoListInterface;

    public ToDoService(ToDoListInterface toDoListInterface) {
        this.toDoListInterface = toDoListInterface;
    }

    public void saveTodo(ToDoList toDoList){
        toDoList.setCompleted(false);
        toDoListInterface.save(toDoList);
    }

    public void setTodoCompleted(ToDoList toDoList){
        ToDoList todo = toDoListInterface.getReferenceById(toDoList.getId());
        todo.setCompleted(!todo.getCompleted());
        toDoListInterface.save(todo);
    }

    public void deleteCompleted(){
        for (ToDoList todos: toDoListInterface.findAll()){
            if (todos.getCompleted()==Boolean.TRUE){
                toDoListInterface.delete(todos);
            }
        }
    }
}
