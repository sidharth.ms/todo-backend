package com.example.backend;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/todo")
public class ToDoController {
    public ToDoListInterface toDoListInterface;

    public ToDoService toDoService;

    public ToDoController(ToDoListInterface toDoListInterface, ToDoService toDoService) {
        this.toDoListInterface = toDoListInterface;
        this.toDoService = toDoService;
    }

    @GetMapping
    @CrossOrigin(origins = "http://localhost:3000")
    public List<ToDoList> getAll(){
        return toDoListInterface.findAll();
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping
    public ResponseEntity<List<ToDoList>> saveToDo(@RequestBody ToDoList toDoList){
        toDoService.saveTodo(toDoList);
        return ResponseEntity.ok(toDoListInterface.findAll());
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping("/complete")
    public ResponseEntity<Boolean> setCompleted(@RequestBody ToDoList todolist){
        toDoService.setTodoCompleted(todolist);
        return ResponseEntity.ok(Boolean.TRUE);
    }


    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/delete")
    public ResponseEntity<Boolean> deleteTodo(){
        toDoService.deleteCompleted();
        return ResponseEntity.ok(true);
    }
}

