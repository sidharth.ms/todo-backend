package com.example.backend;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;


public class TodoServiceTest {
    ToDoListInterface mockInterface;
    ToDoList mockTodo;
    @BeforeEach
    public void beforeEach() {
         mockInterface = mock(ToDoListInterface.class);
         mockTodo = mock(ToDoList.class);
    }
    @Test
    void ShouldBeAbleToSaveTodo() {
        ToDoService toDoService = new ToDoService(mockInterface);

        toDoService.saveTodo(mockTodo);

        verify(mockInterface).save(mockTodo);
        verify(mockTodo).setCompleted(false);
    }

    @Test
    void ShouldBeAbleToCompleteToDo() {
        ToDoService toDoService = new ToDoService(mockInterface);
        when(mockTodo.getCompleted()).thenReturn(false);
        when(mockTodo.getId()).thenReturn(5L);
        when(mockInterface.getReferenceById(anyLong())).thenReturn(mockTodo);

        toDoService.setTodoCompleted(mockTodo);

        verify(mockTodo).setCompleted(true);
        verify(mockInterface).save(mockTodo);
    }

    @Test
    void ShouldBeAbleToDeleteCompleted() {
        ToDoService toDoService = new ToDoService(mockInterface);
        when(mockTodo.getId()).thenReturn(5L);
        List<ToDoList> toDoLists;
        toDoLists = new ArrayList<>();
        toDoLists.add(mockTodo);
        when(mockInterface.findAll()).thenReturn(toDoLists);
        when(mockTodo.getCompleted()).thenReturn(true);

        toDoService.deleteCompleted();

        verify(mockInterface).delete(mockTodo);
    }
}
