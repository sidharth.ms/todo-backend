package com.example.backend;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ToDoListTest {
    @Test
    void ShouldBeAbleToCreateTodo() {
        String discription = "Todo";
        Boolean completed = false;
        ToDoList toDoList = new ToDoList(discription, completed);
        assertEquals(toDoList.getDiscription(),discription);
        assertEquals(toDoList.getCompleted(),completed);
    }

    @Test
    void ShouldBeAbleToAddParameters() {
        String discription = "Discription";
        ToDoList todo = new ToDoList(discription, false);
        assertEquals(todo.getDiscription(),discription);
    }
}
