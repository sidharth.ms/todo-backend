package com.example.backend;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.tools.jconsole.JConsoleContext;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(classes = BackendApplication.class) // Replace App with your main application class
@AutoConfigureMockMvc
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class ToDoControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ToDoListInterface toDoListInterface; // This might need to be mocked depending on your implementation

    @BeforeEach
    public void before(){
        toDoListInterface.deleteAll();
    }
    @AfterEach
    public void after() {
        toDoListInterface.deleteAll();
    }

    @Test
    public void testGetAllTodos() throws Exception {
        // Simulate existing data (optional, modify if needed)
        ToDoList existingToDo = new ToDoList("Existing task", false);
        toDoListInterface.save(existingToDo);

        // Perform the GET request
        mockMvc.perform(MockMvcRequestBuilders.get("/todo"))
                .andExpect(status().isOk())
                .andExpect(content().json("[{'id':2,'discription':'Existing task','completed':false}]"));
    }

    @Test
    public void testSaveToDo() throws Exception {
        // Create a new ToDoList object
        ToDoList newToDo = new ToDoList("New task", false);


        // Perform the POST request with JSON body
        mockMvc.perform(MockMvcRequestBuilders.post("/todo")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(newToDo)))
                .andExpect(status().isOk())
                .andExpect(content().json("[{'id':1,'discription':'New task','completed':false}]"));
    }

    @Test
    void ShouldSetCompleted() throws Exception {
        ToDoList newToDo = new ToDoList("New task", false);
        toDoListInterface.save(newToDo);

        mockMvc.perform(MockMvcRequestBuilders.post("/todo/complete")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(newToDo)))
                .andExpect(status().isOk());
        System.out.println(toDoListInterface.findAll());
        assertEquals(toDoListInterface.findAll().get(0).getCompleted(),true);
    }

    @Test
    void ShouldDeleteCompleted() throws Exception {
        ToDoList newToDo = new ToDoList("New task", true);
        toDoListInterface.save(newToDo);

        mockMvc.perform(MockMvcRequestBuilders.get("/todo/delete")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(newToDo)))
                .andExpect(status().isOk());
        assertEquals(toDoListInterface.findAll().size(),0);
    }


    private static String asJsonString(Object object) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(object);
    }
}

